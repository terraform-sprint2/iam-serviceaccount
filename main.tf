resource "google_service_account" "service_account" {
  account_id   = var.acc_id
  display_name = var.name
}